---
layout: page
titles:
  en: About
  zh: 关于
  zh-Hans: 关于
  zh-Hant: 關於
key: page-about
---

Welcome to my blog! :earth_asia: :earth_africa: :earth_americas:

```java
public class AboutMe {

    public static void main(String[] args) {
        
        String fullName = "Nguyen Dinh Quang";
        String github = "github.com/qndev";
        String linkedIn = "linkedin.com/in/qndev";
        String phone = "(+84) 975 857 109";
        String email = "quangnd.hust@gmail.com";

        System.out.println("My name is:" + fullName);
        System.out.println("Follow me on:" + github + " and " + linkedIn);
        System.out.println("Contact me:" + email + " or " + phone);
    }
    
}
```
